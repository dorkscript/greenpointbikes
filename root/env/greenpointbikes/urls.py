from django.conf.urls.defaults import patterns, include, url
from django.contrib.auth.views import login, logout

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url('^$', 'greenpointbikes.views.index', name="index"),
    url('^beta/$', 'greenpointbikes.views.beta', name="beta-home"),
    url('^contact/$', 'greenpointbikes.views.contact', name="contact"),
    url('^grid/$', 'greenpointbikes.views.grid', name="grid"),
    
    url('^bikes/$', 'used_bikes.views.index', name="bikes"),
    url(r'^bikes/(?P<cat_id>\d+)/(?P<cat_name>[\w|\W]+)/$', 'used_bikes.views.details', name="bike-detail"),
    
    url('^service/$', 'used_bikes.views.service', name="service"),
    url(r'^service/(?P<service_name>[\w|\W]+)/$', 'used_bikes.views.service_details', name="service-detail"),
    
    url('^rentals/$', 'used_bikes.views.rentals', name="rentals"),
    url(r'^rentals/(?P<rentals_name>[\w|\W]+)/$', 'used_bikes.views.rentals_details', name="rentals-detail"),
    
    url(r'^login/$',  login, {'template_name': 'login.html'}),
    url(r'^logout/$', logout),
    
    url(r'^admin/', include(admin.site.urls)),
)
