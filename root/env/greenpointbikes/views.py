from django.shortcuts import render_to_response
from django.http import HttpResponse
from django import http
from django.contrib import auth
from django.contrib.auth.models import User
	
def index(request):
	return render_to_response('_old-site.html', {})

def beta(request):
	return render_to_response('index.html', {'range':range(1,20)})

def contact(request):
	return render_to_response('contact.html', {})

def grid(request):
	cols = 33
	gutter = 20
	width = 10
	rangelst = []
	for i in reversed(xrange(1, cols+1)):
		if cols % i == 0:
			span = []
			rloop = range(cols/i)
			for j in rloop:
				css = "span-%s" %i
				if j == len(rloop)-1:
					css += " last"
				d = D()
				d.css = css
				d.num = i
				span.append(d)
			rangelst.append(span)
	
	rvse = reversed(rangelst)
	x = 0
	for i in rvse:
		if x != 0:
			rangelst.append(i)
		x+=1
	
	return render_to_response('grid.html', {'cols':cols, 'gutter':gutter,'width':width, 'rangelst':rangelst})

class D:
	def __init__(self):
		self.css = ""
		self.num = 0

"""
def signin(request):
	if request.method == 'POST':
		login = request.POST.get('login','')
		pwd = request.POST.get('pass','')
		
		user = auth.authenticate(username=login, password=pwd)
		if user is not None and user.is_active:
			#print "correct"
			auth.login(request, user)
			return http.HttpResponseRedirect('beta')
		else:
			#print "incorrect"
			return render_to_response('signin.html')
		
	else:
		if request.user.is_authenticated():
			return http.HttpResponseRedirect('beta')
	
	return render_to_response('signin.html')
"""

"""
sign out the current user and redirect to login
"""
"""
def signout(request):
	auth.logout(request)
	return render_to_response('signin.html')
"""