from django.db import models

class Menu(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    base_url = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Admin:
        pass

    def __unicode__(self):
        return "%s" % self.name

    def save(self):
        """
        Re-order all items at from 10 upwards, at intervals of 10.
        This makes it easy to insert new items in the middle of 
        existing items without having to manually shuffle 
        them all around.
        """
        super(Menu, self).save()

        current = 10
        for item in MenuItem.objects.filter(menu=self).order_by('order'):
            item.order = current
            item.save()
            current += 10

class MenuItem(models.Model):
    menu = models.ForeignKey(Menu)
    order = models.IntegerField()
    link_url = models.CharField(max_length=100, help_text='URL or URI to the content, eg /about/ or http://foo.com/')
    title = models.CharField(max_length=100)
    login_required = models.BooleanField(blank=True)

    class Admin:
        pass

    def __unicode__(self):
        return "%s %s. %s" % (self.menu.slug, self.order, self.title)

"""
class Nav(models.Model):
    def menu(self):
        menu = []
        menu.append({"text":"Bicycles", "link":"/bicycles"})
        menu.append({"text":"Services", "link":"/services"})
        menu.append({"text":"Fittings", "link":"/fittings"})
        menu.append({"text":"Rentals", "link":"/rentals"})
        menu.append({"text":"Sales", "link":"/sales"})
        menu.append({"text":"Locations", "link":"/locations"})
        menu.append({"text":"About", "link":"/about"})
        menu.append({"text":"Gallery", "link":"/gallery"})
        return menu
"""