from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.conf import settings
import os

register = template.Library()

@register.filter()
def bike_img(bike_images):
    #grab all of the images
    all_images = bike_images.all()
    
    if all_images.count() > 1:
        #grab the first image right now. figure out what we can do for multiple images
        image = all_images[0]
    else:
        #only one image. grab the first
        image = all_images[0]
        
    #print settings.MEDIA_URL + " + " + image.url.rpartition("/content/")[2]
    
    img_url = os.path.join(settings.MEDIA_URL, image.url.rpartition("/content/")[2])
    
    default_path = img_url
    default_alt = "alt"
    default_title = "my title"
    
    img = "<img src=\"%s\" alt=\"%s\" title=\"%s\" />" % (default_path, default_alt, default_title)
    return mark_safe(img)
