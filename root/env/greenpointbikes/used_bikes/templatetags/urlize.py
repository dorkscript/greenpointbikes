from django import template

register = template.Library()

@register.filter()
def urlize(str):
    return str.replace(" ", "-").lower()