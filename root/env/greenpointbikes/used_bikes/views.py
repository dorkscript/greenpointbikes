from django.shortcuts import render_to_response
from django.template import Context, Template, RequestContext
from django.http import HttpResponse
from django.http import Http404
from django import http
from django.contrib import auth
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe

from models import *

def index(request):
    cats = BikeCategory.objects.all()
    cats = cats.order_by('name')
    
    if cats.count() > 0:
        #grab the title of the first category
        title = cats[0].name
        #select the selected category so we can get the related objects
        first_cat = BikeCategory.objects.get(id=cats[0].id)
        
        #get all bikes by the current bike category
        bikes = first_cat.bike_set.all()
        bikes = bikes.filter(is_listed=True)
        bikes = bikes.order_by('-date_listed', 'sort_order')
    else:
        title = "no bikes listed"
        bikes = []
    
    return render_to_response('bike-listings.html', {'title': title, 'bikes':bikes, 'cats':cats})

def details(request, cat_id, cat_name):
    cats = BikeCategory.objects.all()
    cats = cats.order_by('name')
    
    sel_cat = BikeCategory.objects.get(id=cat_id)
    
    if sel_cat:
        #grab the title of the select category
        title = sel_cat.name
        
        #get all bikes by the current bike category
        bikes = sel_cat.bike_set.all()
        bikes = bikes.filter(is_listed=True)
        bikes = bikes.order_by('-date_listed', 'sort_order')
    else:
        raise Http404
    
    return render_to_response('bike-listings.html', {'title': title, 'bikes':bikes, 'cats':cats})

def get_services():
    
    d = dict()
    
    d['index'] = {'template':'services.index.html', 'title': 'Services'}
    d['saftey-check'] = {'template':'services.saftey-check.html', 'title': 'Saftey Check'}
    d['basic-tune'] = {'template':'services.basic-tune.html', 'title': 'Basic Tune'}
    d['performance-tune'] = {'template':'services.performance-tune.html', 'title': 'Performance Tune'}
    d['drive-train-clean'] = {'template':'services.drive-train-clean.html', 'title': 'Drive Train Clean'}
    d['major-tune'] = {'template':'services.major-tune.html', 'title': 'Major Tune'}
    
    return d

def service(request):
    services = get_services()
    page = services['index']
    
    service_links = []
    for k,v in services.iteritems():
        service_links.append(ServiceLink(k, v["title"], v["template"]))
        
    return render_to_response('service.html', {'services':service_links, 'template':page['template'], 'title': page['title']}, context_instance=RequestContext(request))

def service_details(request, service_name):
    
    services = get_services()
    if services.has_key(service_name):
        page = services[service_name]
        
        service_links = []
        for k,v in services.iteritems():
            service_links.append(ServiceLink(k, v["title"], v["template"]))
    else:
        raise 404
    
    return render_to_response('service.html', {'services':service_links, 'template':page['template'], 'title': page['title']}, context_instance=RequestContext(request))

def rentals(request):
    return render_to_response('rentals.html', {})

def rentals_details(request, rentals_name):
    return render_to_response('rentals.html', {})






