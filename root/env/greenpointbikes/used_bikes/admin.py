from used_bikes.models import *
from django.contrib import admin

admin.site.register(Bike)
admin.site.register(Drive_Train)
admin.site.register(Brand)
admin.site.register(Model)
admin.site.register(BikeCategory)
admin.site.register(BikePhoto)