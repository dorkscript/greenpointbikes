from django.db import models
import os.path
from django.conf import settings
import datetime

RESOURCE_DIR = os.path.join(settings.IMAGES_DIR, 'used_bikes/')

class Model(models.Model):
    text = models.CharField(max_length=50)
    
    def __unicode__(self):
        return "%s" % (self.text)

class Brand(models.Model):
    text = models.CharField(max_length=50)
    
    def __unicode__(self):
        return "%s" % (self.text)

class Drive_Train(models.Model):
    text = models.CharField(max_length=50)
    
    def __unicode__(self):
        return "%s" % (self.text)

class BikeCategory(models.Model):
    name = models.CharField(max_length=40, null=False, blank=False)
    sort_order = models.SmallIntegerField(default=100)
    date_listed = models.DateField('dated listed', default=datetime.date.today)
    date_updated = models.DateField('date updated', default=datetime.date.today)
    
    def __unicode__(self):
        return "%s" % self.name

class BikePhoto(models.Model):
    url = models.FilePathField(path=RESOURCE_DIR, match=".*\.jpg$", recursive=True)
    title = models.CharField(max_length=30, null=True, blank=True)
    alt = models.CharField(max_length=30, null=True, blank=True)
    sort_order = models.SmallIntegerField(default=10)
    
    def __unicode__(self):
        filename = os.path.basename(self.url)
        return "%s" % (filename)
"""
class WheelSize(models.Model):
    text = models.CharField(max_length=30)
    #700
    #700C
    #27
    #26
    #24
"""

class Bike(models.Model):
    title = models.CharField(max_length=30)
    brand = models.ForeignKey(Brand)
    model = models.ForeignKey(Model, null=True, blank=True)
    size = models.CharField(max_length=25)
    drive_train = models.ForeignKey(Drive_Train)
    wheel_size = models.CharField(max_length=20, null=True, blank=True)
    price = models.DecimalField(decimal_places=2, max_digits=5)
    has_sold = models.NullBooleanField(null=True,default=False)
    is_listed = models.BooleanField(default=False)
    sort_order = models.SmallIntegerField(default=100)
    categories = models.ManyToManyField(BikeCategory)
    photos = models.ManyToManyField(BikePhoto)
    date_listed = models.DateField('dated listed', default=datetime.date.today)
    date_updated = models.DateField('date updated', default=datetime.date.today)
    
    def __unicode__(self):
        return "%s %s %s %s %s %d" % (self.brand, self.model, self.size, self.drive_train, self.wheel_size, self.price)

class ServiceLink():
    Name = "ServiceLink"
    def __init__(self, key, title, template):
        self.key = key
        self.title = title
        self.template = template
    
    @models.permalink
    def get_absolute_url(self):
        if self.key == "index":
            return ('service')
        else:
            return ('service-detail', [str(self.key)])
    


"""
BEGIN;
CREATE TABLE "used_bikes_model" (
    "id" integer NOT NULL PRIMARY KEY,
    "text" varchar(50) NOT NULL
)
;
CREATE TABLE "used_bikes_brand" (
    "id" integer NOT NULL PRIMARY KEY,
    "text" varchar(50) NOT NULL
)
;
CREATE TABLE "used_bikes_drive_train" (
    "id" integer NOT NULL PRIMARY KEY,
    "text" varchar(50) NOT NULL
)
;
ALTER TABLE "used_bikes_bike" (
    "id" integer NOT NULL PRIMARY KEY,
    "brand_id" integer NOT NULL REFERENCES "used_bikes_brand" ("id"),
    "model_id" integer REFERENCES "used_bikes_model" ("id"),
    "size" varchar(25) NOT NULL,
    "drive_train_id" integer NOT NULL REFERENCES "used_bikes_drive_train" ("id"),
    "wheel_size" varchar(20),
    "price" decimal NOT NULL,
    "has_sold" bool
)
;
COMMIT;

"""